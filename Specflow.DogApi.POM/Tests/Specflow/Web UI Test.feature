﻿@browser
Feature: Specflow With Page Object Model
    In order to see good boi's in a webrowser
    As a test dev
    I want specflow features that use the Page Object Model pattern

@braveBrowser	
Scenario: Test DuckDuckGo Search
    When I navigate to the search page
    And submit a search for 'good boi'
    Then the search should be successful
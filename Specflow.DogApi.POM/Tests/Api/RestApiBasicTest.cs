﻿using System.Net;
using RestSharp;
using NUnit.Framework;
using FluentAssertions;
using RestSharp.Serialization.Json;
using Specflow.DogApi.POM.Framework.Models;

namespace Specflow.DogApi.POM.Tests.Api
{
    [TestFixture]
    public class RestApiBasicTest
    {
        private const string BaseUrl = "https://dog.ceo/";
        private const string Resource = "api/breeds/image/random";
        private JsonDeserializer deserializer = new JsonDeserializer();
        private RestClient endpoint;
        private IRestResponse response;
        
        [SetUp]
        public void InitialiseClient()
        {
            endpoint = new RestClient(BaseUrl);
        }

        [Test]
        public void TestDogApiStatusCode()
        {
            // Act
            var request = new RestRequest(Resource, Method.GET);
            response = endpoint.Execute(request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
        
        [Test]
        public void TestDogApiContent()
        {
            // Act
            var request = new RestRequest(Resource, Method.GET);
            var response = endpoint.Execute(request);

            var dogResponse = deserializer.Deserialize<DogResponse>(response);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            dogResponse.Status.Should().Be("success");
            dogResponse.Message.Should().NotBeNullOrWhiteSpace();
        }
    }
}
﻿using System.Net;
using FluentAssertions;
using RestSharp;
using NUnit.Framework;
using RestSharp.Serialization.Json;
using Specflow.DogApi.POM.Framework.Models;
using static Specflow.DogApi.POM.Framework.WebService.DogImageRequest;

namespace Specflow.DogApi.POM.Tests.Api
{
    public class RestApiAdvancedTest
    {
        private const string BaseUrl = "https://dog.ceo/";
        private const string Resource = "api/breeds/image/random";
        
        private RestClient dogEndpoint;
        private IRestResponse response;
        private JsonDeserializer deserializer = new JsonDeserializer();
        
                
        [SetUp]
        public void InitialiseClient()
        {
            dogEndpoint = new RestClient(BaseUrl);
        }

        [Test]
        public void TestDogApi()
        {
            // Act
            var request = new RestRequest(Resource, Method.GET);
            response = dogEndpoint.Execute(request);

            var dogResponse = deserializer.Deserialize<DogResponse>(response);
            
            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            dogResponse.Status.Should().Be("success");
            dogResponse.Message.Should().NotBeNullOrWhiteSpace();
        }

        [Test]
        public void TestDogApiImage()
        {
            // Act: call API and get dog image
            var imageData = GetImage();

            // Assert for good boi!
            imageData.Should().NotBeNullOrEmpty();
        }

    }
}
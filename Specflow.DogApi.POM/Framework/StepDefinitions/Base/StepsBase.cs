﻿using Specflow.DogApi.POM.Framework.PageObjects;
using TechTalk.SpecFlow;

namespace Specflow.DogApi.POM.Framework.StepDefinitions.Base
{
    [Binding]
    public class StepsBase : Steps
    {
        private readonly ScenarioContext _scenarioContext;

        private readonly FeatureContext _featureContext;


        public StepsBase(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
    
        #region Common Page Objects
        protected SearchPage SearchPage => _scenarioContext.ScenarioContainer.Resolve<SearchPage>();

        protected ResultPage ResultPage => _scenarioContext.ScenarioContainer.Resolve<ResultPage>();

        #endregion
    }
}
﻿using FluentAssertions;
using Specflow.DogApi.POM.Framework.StepDefinitions.Base;
using TechTalk.SpecFlow;

namespace Specflow.DogApi.POM.Framework.StepDefinitions
{
    [Binding]
    public sealed class WebUiTest : StepsBase
    {
        private static ScenarioContext _scenarioContext;

        public WebUiTest(ScenarioContext scenarioContext) : base(scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        
        #region Step Methods
       [When(@"I navigate to the search page")]
        public void WhenINavigateToTheSearchPage()
        {
            SearchPage.NavigateTo("https://www.duckduckgo.com");
        }

        [When(@"submit a search for '(.*)'")]
        public void WhenSubmitASearchFor(string scenarioSearch)
        {
            SearchPage.EnterSearchText(scenarioSearch);
            SearchPage.ClickSearchButton();
        }

        [Then(@"the search should be successful")]
        public void ThenTheSearchShouldBeSuccessful()
        {
            ResultPage.VerifyResultLinks().Should().BeTrue();
        }
        
        #endregion
    }
}
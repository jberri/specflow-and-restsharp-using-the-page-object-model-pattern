﻿using OpenQA.Selenium;
using Specflow.DogApi.POM.Framework.PageObjects.Base;
using TechTalk.SpecFlow;

namespace Specflow.DogApi.POM.Framework.PageObjects
{
    public class SearchPage : PageBase
    {
        private static ScenarioContext _scenarioContext;
        
        public SearchPage(PageContext pageContext, ScenarioContext scenarioContext) : base(pageContext, scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        
        #region Page Elements
        
        private IWebElement SearchInput => GetElement(By.Id("search_form_input_homepage"));
        private IWebElement SearchButton => GetElement(By.Id("search_button_homepage"));
        
        #endregion
        
        #region Page Interaction
        
        public void EnterSearchText(string scenarioText)
        {
            // Use the scenario search string and provide a fallback
            string searchTerm = (!string.IsNullOrWhiteSpace(scenarioText)) ? scenarioText : "bad dog!";
            
            EnterText(SearchInput, searchTerm);
        }
        public void ClickSearchButton()
        {
            JavascriptClick(SearchButton);
        }
        
        #endregion
    }
}
﻿using Specflow.DogApi.POM.Framework.PageObjects.Base;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace Specflow.DogApi.POM.Framework.PageObjects
{
    public class ResultPage : PageBase
    {
        private static ScenarioContext _scenarioContext;
        
        public ResultPage(PageContext pageContext, ScenarioContext scenarioContext) : base(pageContext, scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }
        
        #region Page Elements
        
        private IWebElement ResultLinks => GetElement(By.ClassName("result__a"));
        
        #endregion

        public bool VerifyResultLinks()
        {
            return IsElementVisible(ResultLinks);
        }
    }
}
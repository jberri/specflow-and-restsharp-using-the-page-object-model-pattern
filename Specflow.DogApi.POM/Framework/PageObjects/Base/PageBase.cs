﻿using System;
using System.Threading;
using FluentAssertions;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace Specflow.DogApi.POM.Framework.PageObjects.Base
{
    public class PageBase
    {
        private readonly PageContext PageContext;
        private static ScenarioContext _scenarioContext;
        private const int MaxWaitSeconds = 15;

        public PageBase(PageContext pageContext, ScenarioContext scenarioContext)
        {
            PageContext = pageContext;
            _scenarioContext = scenarioContext;
        }

        #region Common methods
        public void NavigateTo(string url, int sleep = 0)
        {
            Sleep(sleep);
            PageContext.Driver.Navigate().GoToUrl(url);
        }
        public bool IsElementVisible(IWebElement element)
        {
            return element.Displayed && element.Enabled;
        }
        public IWebElement GetElement(By byLocatorType)
        {
            return WaitForVisibility(byLocatorType);
        }
        public void EnterText(IWebElement element, string textToEnter, int sleep = 0, bool addEnter = false)
        {
            if (!string.IsNullOrWhiteSpace(textToEnter))
            {
                if (element == null)
                {
                    throw new NoSuchElementException();
                }
                else
                {
                    // Clear field of existing test and then enter text
                    Sleep(sleep);
                    JavascriptClick(element);
                    element.Clear();
                    element.SendKeys(textToEnter);

                    // Optional extra "Enter" keypress
                    if (addEnter)
                    {
                        element.SendKeys(Keys.Enter);
                    }
                }
            }
        }
        public IWebElement WaitForVisibility(By byLocatorType, int maxWaitSeconds = MaxWaitSeconds)
        {
            IWebElement element = null;
            var wait = new WebDriverWait(PageContext.Driver, TimeSpan.FromSeconds(maxWaitSeconds));

            element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists(byLocatorType));

            if (element == null)
            {
                throw new ElementNotVisibleException($"Element {byLocatorType} does not exist");
            }

            return element;
        }
        public void ScrollElementIntoView(IWebElement pageElement)
        {
            pageElement.Should().NotBeNull();

            var element = pageElement != null ? (RemoteWebElement)pageElement: null;
            element.Should().NotBeNull();

            IJavaScriptExecutor js = (IJavaScriptExecutor)PageContext.Driver;
            js.ExecuteScript("arguments[0].scrollIntoViewIfNeeded(true);", element);
        }
        public void JavascriptClick(IWebElement element, bool scrollIntoView = false, int sleep = 0)
        {
            Sleep(sleep);

            if (scrollIntoView) { ScrollElementIntoView(element); }

            IJavaScriptExecutor js = (IJavaScriptExecutor)PageContext.Driver;
            js.ExecuteScript("arguments[0].scrollIntoView()", element);
            js.ExecuteScript("arguments[0].click()", element);
        }

        #endregion

        #region Helpers
        public static void Sleep(int sleep)
        {
            Thread.Sleep(sleep);
        }

        #endregion
    }
}
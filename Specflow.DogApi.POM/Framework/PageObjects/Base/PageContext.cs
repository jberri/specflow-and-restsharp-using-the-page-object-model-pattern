﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace Specflow.DogApi.POM.Framework.PageObjects.Base
{
    public class PageContext
    {
        private static readonly ThreadLocal<IWebDriver> ContextDriver = new ThreadLocal<IWebDriver>();

        public PageContext(IWebDriver driver)
        {
            Driver = driver;
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
        }
        public IWebDriver Driver
        {
            get => ContextDriver.Value;
            private set => ContextDriver.Value = value;
        }
    }
}
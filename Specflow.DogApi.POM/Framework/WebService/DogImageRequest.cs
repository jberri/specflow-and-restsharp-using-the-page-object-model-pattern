﻿using System;
using System.IO;
using System.Reflection;
using RestSharp;
using RestSharp.Serialization.Json;
using Specflow.DogApi.POM.Framework.Models;

namespace Specflow.DogApi.POM.Framework.WebService
{
    public static class DogImageRequest
    {
        private const string BaseUrl = "https://dog.ceo/";
        private const string ImagesUrl = "https://images.dog.ceo/";
        private const string DownloadToken = "DogImagesApiDownload";
        private static readonly string downloadDir = $"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\{DownloadToken}";
        private static readonly string fileName = "goodBoi";
        
        private static RestClient dogEndpoint;
        private static RestClient imagesEndpoint;
        private static IRestResponse response;
        private static JsonDeserializer deserializer = new JsonDeserializer();
        private static IRestRequest GetRandomDog() => new RestRequest("api/breeds/image/random", Method.GET);

        public static byte[] GetImage()
        {
            // Make api call and store typed response
            var dogEndpoint = new RestClient(BaseUrl);
            var dogRequest = GetRandomDog();
            var dogResponse = deserializer.Deserialize<DogResponse>(dogEndpoint.Execute(dogRequest));

            // Decompose response
            var imagesEndpoint = new RestClient(ImagesUrl);
            var resource = new Uri(dogResponse.Message).AbsolutePath;
            var imageRequest = new RestRequest(resource, Method.GET);
            var extension = Path.GetExtension(resource);

            // Make images api call
            byte[] imageData = imagesEndpoint.DownloadData(imageRequest);
            var timeStamp = CurrentDateTime();

            // Download image
            CreateDownloadDirectory();
            File.WriteAllBytes($"{downloadDir}\\{fileName}_{timeStamp}{extension}", imageData);
            
            return imageData;
        }
        
        private static string CurrentDateTime()
        {
            return DateTime.Now.ToString("yyyy.MM.dd_HH.mm.ss");
        }
        
        private static void CreateDownloadDirectory()
        {
            if (!Directory.Exists(downloadDir))
            {
                Directory.CreateDirectory(downloadDir);
            }
        } 
        
    }
}
﻿using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using StringComparison = System.StringComparison;

namespace Specflow.DogApi.POM.Framework.WebDriver
{
    public static class BrowserContext
    {
        private static readonly ThreadLocal<IWebDriver> ContextDriver = new ThreadLocal<IWebDriver>();
        public static IWebDriver Driver
        {
            get => ContextDriver.Value;
            private set => ContextDriver.Value = value;
        }
        public static void InitialiseWebDriver(string browserName)
        {
            switch (browserName)
            {
                case var brave when string.Equals(brave, "bravebrowser",  StringComparison.InvariantCultureIgnoreCase):
                    SetupBraveBrowserDriver();
                    break;
                case var chrome when string.Equals(chrome, "chromebrowser", StringComparison.InvariantCultureIgnoreCase):
                    SetupChromeWebDriver();
                    break;
                default:
                    SetupBraveBrowserDriver();
                    break;
            }
        }
        private static void SetupBraveBrowserDriver()
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;
            
            var options = new ChromeOptions();
            options.BinaryLocation = "C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe";
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--incognito");
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-extensions");
            
            Driver = new ChromeDriver(service, options);
        }
        private static void SetupChromeWebDriver()
        {
            var service = ChromeDriverService.CreateDefaultService();
            service.HideCommandPromptWindow = true;

            var options = new ChromeOptions();
            options.AddArgument("--no-sandbox");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--incognito");
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-extensions");

            Driver = new ChromeDriver(service, options);
        }
        public static void ClearUpWebDriver()
        {
            if (Driver != null)
            {
                Driver.Quit();
                Driver.Dispose();
                Driver = null;
            }
        }
    }
}
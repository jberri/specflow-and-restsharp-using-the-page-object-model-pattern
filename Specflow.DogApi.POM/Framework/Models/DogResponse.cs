﻿namespace Specflow.DogApi.POM.Framework.Models
{
    public class DogResponse
    { 
        public string Message { get; set; }

        public string Status { get; set; }
    }
}
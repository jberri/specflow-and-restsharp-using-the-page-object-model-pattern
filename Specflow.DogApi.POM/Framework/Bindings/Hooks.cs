﻿using System;
using System.Linq;
using BoDi;
using FluentAssertions;
using Specflow.DogApi.POM.Framework.PageObjects.Base;
using TechTalk.SpecFlow;
using static Specflow.DogApi.POM.Framework.WebDriver.BrowserContext;

namespace Specflow.DogApi.POM.Framework.Bindings
{
    [Binding]
    public sealed class Hooks
    {
        private readonly IObjectContainer _objectContainer;
        private static ScenarioContext _scenarioContext;
        private static FeatureContext _featureContext;
        
        public Hooks(IObjectContainer objectContainer, ScenarioContext scenarioContext, FeatureContext featureContext)
        {
            _scenarioContext = scenarioContext;
            _featureContext = featureContext;
            _objectContainer = objectContainer;
        }
        
        #region hooks
        
        [Scope(Tag = "browser", Feature = "Specflow With Page Object Model")]
        [BeforeScenario]
        public void StartWebDriver()
        {
            var browserName = GetBrowserNameFromScenarioTag();

            if (!string.IsNullOrWhiteSpace(browserName))
            {
                InitialiseWebDriver(browserName);
            }
            
            _objectContainer.RegisterInstanceAs(new PageContext(Driver));
        }
        
        [AfterScenario]
        public void Teardown()
        {
            ClearUpWebDriver();
        }
        
        #endregion
       
        #region private helpers
        private string GetBrowserNameFromScenarioTag()
        {
            return GetTokenByScenarioTag("browser");
        }
        private string GetTokenByScenarioTag(string tagName)
        {
            string result = null;
            
            if (ScenarioTagIsPresent(tagName))
            {
                // Assert scenario has only a single 'browser' tag
                (GetScenarioTags() ?? Array.Empty<string>())
                    .Count(tag => tag.Contains(tagName)).Should().BeLessThan(2,
                        $"because there should only be a single @{tagName} tag per scenario.");

                return (GetScenarioTags() ?? Array.Empty<string>())
                    .FirstOrDefault(t => t.Contains(tagName, StringComparison.CurrentCultureIgnoreCase))!;
            }
            else
            {
                return result;
            }
        }
        private bool FeatureTagIsPresent(string tagName)
        {
            return GetFeatureTags()
                .Any(tag => tag.Contains(tagName, StringComparison.CurrentCultureIgnoreCase));
        }
        private bool ScenarioTagIsPresent(string tagName)
        {
            return (GetScenarioTags() ?? Array.Empty<string>())
                .Any(tag => tag.Contains(tagName, StringComparison.CurrentCultureIgnoreCase));
        }
        private string[] GetScenarioTags()
        {
            return _scenarioContext.ScenarioInfo.Tags ?? Array.Empty<string>();;
        }
        private string[] GetFeatureTags()
        {
            return _featureContext.FeatureInfo.Tags ?? Array.Empty<string>();
        }

        #endregion
    }
}